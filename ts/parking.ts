class Parking {
    protected _element: HTMLImageElement = document.createElement('img');
    private element: HTMLElement;
    private _parking: string;
    protected _xPos: number;
    protected _yPos: number;

    
    constructor(naam: string, xpos: number, ypos: number) {
        this._parking = naam;

        this._xPos = xpos;
        this._yPos = ypos;
    }

    public draw(){
        const container = document.getElementById('container');
        this._element.id = "parking";
        container.appendChild(this._element);
    }

}