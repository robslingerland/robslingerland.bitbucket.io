class Game {
    private _element: HTMLElement;
    private _background: HTMLImageElement;
    private _parking: Parking;
    private _cars: Array<Car> 
    private _car: Car;
    private _turn: number;
    private _players: Array<Player>;

    constructor() {
        this._element = document.getElementById('container');
        this._parking = new Parking("parking", 0, 0);
        this._turn = 0;
        this._players = new Array();
        this._cars = new Array();

        this._cars[0] = new Car("BMW", 90);
        this._cars[1] = new Car("FORD", 100);

        this._players[0] = new Player(0,"jantje", 0);
        this._players[1] = new Player(1,"pietje", 0);

        //draw is initial state
        this.draw();

        //add keydown handler to the window object
        window.addEventListener('keydown', this.keyDownHandler);
        window.addEventListener("transitionend", this.transitionHandler);
    }

    private draw() {
        for (let i = 0; i < this._cars.length; i++) {
            this._cars[i].setX(800);
            this._cars[i].setY(35);          
        }
        this._element.innerHTML = '';
        let sb = document.createElement("p");
        sb.className = "sb";
        sb.innerHTML += this._players[0].score;
        sb.innerHTML += "<br>";
        sb.innerHTML += this._players[1].score;
        this._element.appendChild(sb);
        this._parking.draw();
        let win = document.createElement("p");
        win.setAttribute("id", "win");
        this._element.appendChild(win);
        this.currentCar();
//        this.collision();
        
    }

    keyDownHandler = (e:KeyboardEvent): void => {
        switch(e.keyCode) {
            case 37:
                this._cars[0].xPos = -30;
                this._element.classList.add('drive');
                break;
            case 39:
                this._cars[1].xPos = -50;
                this._element.classList.add('drive');
                break;
        }
    }

    transitionHandler = (e:TransitionEvent): void => {
        this.collision();
    }

    private currentCar() {
        var randomCar = Math.floor(Math.random() * this._cars.length);
            

        this._cars.map((car, index) => {
            if(index == randomCar){
                car.draw();
            }
        })
    }

    public collision(): void {
        const carRect = document.getElementsByClassName('cars')[0].getBoundingClientRect();
        const Parking = document.getElementById('parking').getBoundingClientRect();
        if (carRect.left <= Parking.right && carRect.left >= Parking.left && carRect.right <= Parking.right) {
            if (this._turn == 0){
                this._players[0].addscore(1);
                this._turn = 1;
            }else{
                this._players[1].addscore(1);
                this._turn = 0;
            }
            console.log("player " + (this._turn+1));
            this.draw();
        }else if(carRect.left <= Parking.left){
            // console.log("je bent er voorbij");
            if (this._turn == 0){
                this._turn = 1;
                document.getElementById("win").style.opacity = "1";
                document.getElementById("win").innerHTML = "je bent er voorbij <br> De volgende is aan de beurt!" + this._players[1].name;   
                
            }else{
                this._turn = 0;
                document.getElementById("win").style.opacity = "1";
                document.getElementById("win").innerHTML = "je bent er voorbij <br> De volgende is aan de beurt!" + this._players[0].name;   
            }
            // setTimeout(this.draw(), 3000);
            //this.draw();
            setTimeout(() => {
                this.draw();
            }, 3000);
            
        }
    }
}