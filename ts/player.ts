class Player{
     private _id: number;
     private _name: string;
     private _scoreboard: number;

    constructor(id:number, name:string, scoreboard:number){
        this._id = id;
        this._name = name;
        this._scoreboard = scoreboard;
    }

    public get name() : string {
        return this._name;
    }

    public get score() : string {
        return this._name + ": " + this._scoreboard;
    }
    
    
    public addscore(x:number) {
        this._scoreboard += x;
    }
    
}